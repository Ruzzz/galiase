# Note: common for api and spider
alembic==1.4.2
mypy==0.782
pylint-quotes==0.2.1
pylint==2.5.3
pytest-cov==2.10.0
pytest==5.4.3
