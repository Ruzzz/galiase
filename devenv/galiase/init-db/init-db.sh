#!/bin/bash
set -e
set -u

function create_user_and_database() {
	local database=$(echo "$1" | tr ',' ' ' | awk  '{print $1}')
	local owner=$(echo "$1" | tr ',' ' ' | awk  '{print $2}')
	local password=$(echo "$1" | tr ',' ' ' | awk  '{print $2}')
	echo "- CREATING DATABASE: '$database' USER: '$owner' -"
	psql -v ON_ERROR_STOP=1 --username "$POSTGRES_USER" <<-EOSQL
	    CREATE USER $owner WITH SUPERUSER PASSWORD '$password';
	    CREATE DATABASE $database OWNER $owner;
EOSQL
}

if [ -n "$POSTGRES_MULTIPLE_DATABASES" ]; then
	echo "- CREATE MULTIPLE DATABASES -"
	for db in $(echo $POSTGRES_MULTIPLE_DATABASES | tr ';' ' '); do
		create_user_and_database $db
	done
fi
