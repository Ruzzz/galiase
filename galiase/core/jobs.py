import asyncio
import logging
from abc import abstractmethod
from typing import Optional

import aioamqp
import ujson
from aioamqp.channel import Channel as AmqpChannel
from aioamqp.protocol import AmqpProtocol

_logger = logging.getLogger(__name__)


class AmqpJobsQueue:
    __slots__ = (
        '_server_url',
        '_queue_id',
        '_prefetch_count',
        '_loop',
        '_transport',
        '_protocol',
        '_channel',
    )

    def __init__(self, server_url, queue_id, *, prefetch_count=1, loop=None):
        self._server_url = server_url
        self._queue_id = queue_id
        self._prefetch_count = prefetch_count
        self._loop = loop or asyncio.get_event_loop()
        self._transport = None
        self._protocol: Optional[AmqpProtocol] = None
        self._channel: Optional[AmqpChannel] = None

    async def setup(self):
        if self._channel is None:
            self._transport, self._protocol = await aioamqp.from_url(
                self._server_url, loop=self._loop, login_method='PLAIN')
            self._channel = await self._protocol.channel()
            await self._channel.basic_qos(prefetch_count=self._prefetch_count)
            await self._channel.queue_declare(queue_name=self._queue_id, durable=True)

    async def publish(self, data: dict):
        await self._channel.basic_publish(  # type: ignore
            payload=ujson.dumps(data),
            exchange_name='',
            routing_key=self._queue_id,
            properties={
                'delivery_mode': 2,
                'content_type': 'application/json'
            })

    async def close(self):
        if self._channel is not None:
            tmp = self._channel
            self._channel = None
            if tmp.is_open:
                await tmp.close()

        if self._protocol is not None:
            tmp = self._protocol
            self._protocol = None
            await tmp.close()

        if self._transport is not None:
            tmp = self._transport
            self._transport = None
            tmp.close()


class AmqpJobsConsumerAbstract(AmqpJobsQueue):
    __slots__ = []  # type: ignore
    valid_content_type: Optional[str] = 'application/json'

    async def __call__(self):

        async def cancel(channel, delivery_tag, consumer_tag, err):
            await channel.basic_client_nack(delivery_tag=delivery_tag)
            await channel.basic_cancel(consumer_tag=consumer_tag, no_wait=True)
            await self.close()
            _logger.exception(str(err), exc_info=err)

        async def cb(channel, json_body, envelope, properties):
            if not self.valid_content_type or self.valid_content_type == properties.content_type:
                params = ujson.loads(json_body)
                try:
                    if await self._on_job(params):
                        await channel.basic_client_ack(delivery_tag=envelope.delivery_tag)
                    else:
                        await channel.basic_client_nack(delivery_tag=envelope.delivery_tag)
                except Exception as err:  # pylint: disable=broad-except
                    self._loop.create_task(cancel(channel, envelope.delivery_tag, envelope.consumer_tag, err))

        _logger.debug('start listen jobs')
        await self._channel.basic_consume(callback=cb, queue_name=self._queue_id, no_ack=False, no_wait=True)

    @abstractmethod
    async def _on_job(self, data: dict) -> bool:
        pass
