from fastapi import HTTPException
from pydantic import BaseModel, conint, HttpUrl, ValidationError, BaseConfig, AnyUrl
from pydantic.fields import ModelField

from galiase.core.urls import UrlValidator

_url_validator = UrlValidator()


class HttpUrlExSchema(HttpUrl):

    @classmethod
    def validate(cls, value: str, field: ModelField, config: BaseConfig) -> AnyUrl:
        ret = _url_validator(value)
        if not ret:
            raise ValueError('Invalid URL')
        return HttpUrl.validate(ret, field, config)


class BaseDependsModel(BaseModel):

    @classmethod
    def _depends(cls, **kwargs):
        try:
            return cls(**kwargs)
        except ValidationError as e:
            errors = e.errors()
            for error in errors:
                error['loc'] = ['query'] + list(error['loc'])
            raise HTTPException(422, detail=errors)


class IndexUrlSchema(BaseDependsModel):
    url: HttpUrlExSchema
    depth: conint(ge=0, le=3) = 0  # type: ignore
    domain_only: bool = True
    skip_recently: bool = True

    @classmethod
    def depends(cls, url, depth=0, domain_only=True, skip_recently=True):
        return cls._depends(
            url=url,
            depth=depth,
            domain_only=domain_only,
            skip_recently=skip_recently)
