from pathlib import Path
from urllib.parse import urlparse, urlunparse

# pylint: disable=too-many-lines
from typing import Optional

_VALID_SCHEMES = {
    'http',
    'https',
}

_SKIP_HOST_SUFFIXES = {
    '.gov',
    '.edu',
    'vk.com',
}

_SKIP_PATH_SUFFIXES = {
    '.css',
    '.csv',
    '.doc',
    '.jpg',
    '.js',
    '.pdf',
    '.png',
    '.tsv',
    '.woff2',
}


def _init_tld():
    # From https://data.iana.org/TLD/tlds-alpha-by-domain.txt
    with open(Path(__file__).parent / 'tlds-alpha-by-domain.txt') as fp:
        ret = (x.strip().lower() for x in fp.readlines() if x)
        return set(filter(lambda x: x and not x.startswith('#'), ret))


VALID_TLD = _init_tld()


def _valid_tld(domain: Optional[str]) -> bool:
    if domain and '.' in domain:
        top_part = domain.rsplit('.', 1)[1]
        return bool(top_part) and (top_part in VALID_TLD)
    return False


def _valid_url(url: Optional[str]) -> bool:
    # pylint: disable=too-many-return-statements
    if not url:
        return False

    for s in _SKIP_PATH_SUFFIXES:
        if url.endswith(s):
            return False

    parts = urlparse(url)

    if parts.scheme not in _VALID_SCHEMES:
        return False

    hostname = parts.hostname
    if hostname:
        if not _valid_tld(hostname):
            return False

        for s in _SKIP_HOST_SUFFIXES:
            if hostname.endswith(s):
                return False
    return True


def prepare_url(url: str, source_url: str = None) -> str:
    scheme_hack = False
    if '://' not in url:
        url = 'http://' + url  # fix urlparse
        scheme_hack = True

    parts = list(urlparse(url.strip()))

    if scheme_hack:
        parts[0] = ''
    else:
        parts[0] = parts[0].lower()  # scheme
    parts[1] = parts[1].lower()  # netloc
    parts[5] = ''  # remove fragment

    if source_url:
        source_parts = urlparse(source_url.strip().lower())
        if not parts[1]:
            parts[1] = source_parts.netloc
        if not parts[0] and parts[1] == source_parts.netloc:
            parts[0] = source_parts.scheme

    if not parts[0] and parts[1]:
        parts[0] = 'http'

    return urlunparse(parts)


class UrlValidator:

    def __init__(self, source_url: str = None):
        self.source_url = source_url

    def __call__(self, url: str) -> Optional[str]:
        # prepare url
        url = prepare_url(url, self.source_url)

        # check url
        if not _valid_url(url):
            return None

        return url
