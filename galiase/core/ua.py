import operator
import random
from functools import reduce
from typing import List

# pylint: disable=line-too-long

# https://github.com/fabienvauchelles/scrapoxy/blob/master/server/proxies/manager/useragent/index.js
# http://techblog.willshouse.com/2012/01/03/most-common-user-agents/


class _BaseValue:
    _TML = '{0}'
    _VALUES = None

    def __init__(self):
        self._len = reduce(operator.mul, (len(x) for x in self._VALUES), 1)

    def _get_indexes(self, main_index: int) -> List[int]:
        ret = []
        for x in reversed(self._VALUES):
            if main_index == 0:
                break
            main_index, i = divmod(main_index, len(x))
            ret.append(i)
        ret.extend([0] * (len(self._VALUES) - len(ret)))
        return list(reversed(ret))

    def __str__(self):
        return random.choice(self)

    def __getitem__(self, item):
        indexes = self._get_indexes(item)
        values = [str(self._VALUES[i][x]) for i, x in enumerate(indexes)]
        return self._TML.format(*values)

    def __len__(self):
        return self._len


class _Amd64(_BaseValue):
    _VALUES = ('x86_64', 'amd64'),


_ARCH1 = 'i586', 'i686', _Amd64()


class _LinuxOs(_BaseValue):
    _TML = 'X11; {0} {1}'
    _VALUES = ('Linux', 'OpenBSD', 'NetBSD', 'Ubuntu; Linux'), _ARCH1,


class _WindowsOs(_BaseValue):
    _TML = 'Windows NT {}{}'
    _VALUES = (
        ('5.0', '5.1', '6.0', '6.1', '6.2', '6.3', '6.4', '10.0'),
        ('; WOW64', '; Win64; x64', '')
    )


class _MacOs(_BaseValue):
    _TML = 'Macintosh; Intel Mac OS X {}'
    _VALUES = (
        (
            '10.6',
            '10_7_5',
            '10.8',
            '10_8_0',
            '10_8_2',
            '10_8_3',
            '10_9',
            '10_9_0',
            '10_9_2',
            '10_9_3',
            '10_10',
            '10_10_1',
            '10.11',
            '10_11',
            '10.12',
            '10_12',
        ),
    )


class _Firefox(_BaseValue):
    _TML = 'Mozilla/5.0 ({os}; rv:{rv}) Gecko/{dt} Firefox/{ver}'
    _VALUES = (
        (
            ('14.0', '20100101', '18.0.1'),
            ('17.0', '20100101', '17.0.6'),
            ('18.0', '20100101', '18'),
            ('21.0', '20100101', '21.0'),
            ('21.0', '20130331', '21.0'),
            ('21.0', '20130326', '21.0'),
            ('21.0', '20130401', '21.0'),
            ('16.0.1', '20121011', '21.0.1'),
            ('22.0', '20130405', '22.0'),
            ('22.0', '20130405', '23.0'),
            ('24.0', '20100101', '24.0'),
            ('25.0', '20100101', '25.0'),
            ('25.0', '20100101', '29.0'),
            ('27.0', '20121011', '27.0'),
            ('27.3', '20130101', '27.3'),
            ('31.0', '20100101', '31.0'),
            ('33.0', '20100101', '33.0'),
            ('36.0', '20100101', '36.0'),
            ('40.0', '20100101', '40.1'),
            ('43.0', '20100101', '43.0'),
            ('45.0', '20100101', '45.0'),
            ('46.0', '20100101', '46.0'),
            ('47.0', '20100101', '47.0'),
            ('48.0', '20100101', '48.0'),
            ('49.0', '20100101', '49.0'),
            ('50.0', '20100101', '50.0'),
        ),
        (_LinuxOs(), _WindowsOs(), _MacOs()),
    )

    def __getitem__(self, item):
        indexes = self._get_indexes(item)
        values = [self._VALUES[i][x] for i, x in enumerate(indexes)]
        return self._TML.format(
            rv=values[0][0],
            dt=values[0][1],
            ver=values[0][2],
            os=str(values[1])
        )


class _Chrome(_BaseValue):
    _TML = 'Mozilla/5.0 ({os}) AppleWebKit/{wk_ver} (KHTML, like Gecko) Chrome/{ver} Safari/{wk_ver}'
    _VALUES = (
        (
            '24.0.1292.0',
            '24.0.1295.0',
            '24.0.1309.0',
            '24.0.1312.60',
            '27.0.1453.90',
            '27.0.1453.93',
            '27.0.1453.116',
            '27.0.1500.55',
            '28.0.1464.0',
            '28.0.1467.0',
            '28.0.1468.0',
            '29.0.1547.2',
            '29.0.1547.57',
            '29.0.1547.62',
            '30.0.1599.17',
            '31.0.1623.0',
            '31.0.1650.16',
            '32.0.1664.3',
            '32.0.1667.0',
            '33.0.1750.517',
            '34.0.1847.116',
            '34.0.1847.137',
            '34.0.1866.237',
            '35.0.1916.47',
            '35.0.2117.157',
            '35.0.2309.372',
            '35.0.3319.102',
            '36.0.1944.0',
            '36.0.1985.67',
            '36.0.1985.125',
            '37.0.2049.0',
            '37.0.2062.124',
            '40.0.2214.93',
            '41.0.2224.3',
            '41.0.2225.0',
            '41.0.2226.0',
            '41.0.2227.0',
            '41.0.2227.1',
            '41.0.2228.0',
            '86.0.4240.111',
        ),
        ('537.17', '537.36'),
        (_LinuxOs(), _WindowsOs(), _MacOs()),
    )

    def __getitem__(self, item):
        indexes = self._get_indexes(item)
        values = [self._VALUES[i][x] for i, x in enumerate(indexes)]
        return self._TML.format(
            ver=values[0],
            wk_ver=values[1],
            os=str(values[2])
        )


class _UserAgent(_BaseValue):
    _VALUES = (_Chrome(), _Firefox()),


def random_user_agent():
    return random.choice(_UserAgent())
