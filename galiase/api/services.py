import logging
from datetime import datetime, timezone

from typing import List

from sqlalchemy import func, desc, asc

from galiase import conf
from galiase.api.defs import Stats
from galiase.api.schemas import DocumentSchema
from galiase.core.jobs import AmqpJobsQueue
from galiase.models.meta import db
from galiase.models.models import Document

logger = logging.getLogger(__name__)


class DocumentsService:
    _S2TS_FN = func.plainto_tsquery

    def __init__(self):
        self._queue = AmqpJobsQueue(conf.GALIASE_JOBS_URI, conf.GALIASE_JOBS_QUEUE)
        self._statistics = Stats(dt=None)

    async def setup(self):
        await self._queue.setup()

    async def close(self):
        await self._queue.close()

    async def index_url(self, url: str, depth: int, domain_only: bool, skip_recently: bool):
        query = Document.select('updated', 'last_error').where(Document.url == url)
        status_item = await query.gino.one_or_none()
        if status_item is not None:
            updated = status_item['updated']
            status = status_item['last_error']
            if not status:
                status = 'indexed'
            status += ', check and try reindex..'
        else:
            status = 'added to queue'
            updated = None

        await self._queue.publish({
            'url': url,
            'depth': depth,
            'domain_only': domain_only,
            'skip_recently': skip_recently
        })
        return {
            'url': url,
            'status': status,
            'updated': updated,
        }

    def _fts_where_q(self, s: str):
        # return func.to_tsvector('english', Document.text).match(s, postgresql_regconfig='english')
        # return func.to_tsvector('english', Document.text).op('@@')(func.plainto_tsquery('english', s))
        return Document.text_fts.op('@@')(self._S2TS_FN('english', s))

    async def total(self, s: str) -> int:
        query = db.select([func.count(Document.id)]).where(self._fts_where_q(s))  # pylint: disable=no-member
        return await query.gino.scalar()

    async def search(self, s: str, *,
                     limit: int = 10, offset=0,
                     sort_rank=True, text_limit=200) -> List[DocumentSchema]:
        fields = 'id', 'title', 'url', 'text', 'updated'
        query = Document.select(*fields).where(self._fts_where_q(s)).limit(limit).offset(offset)

        if sort_rank:
            order_by_q = desc(func.ts_rank_cd(Document.text_fts, self._S2TS_FN('english', s)))
        else:
            order_by_q = asc(Document.title)
        query = query.order_by(order_by_q)

        def prepare_item_(x_):
            item_ = DocumentSchema.from_orm(x_)
            item_.text = item_.text[:text_limit]
            return item_

        return [prepare_item_(x) for x in await query.gino.all()]

    async def statistics(self) -> Stats:
        now = datetime.now(timezone.utc)
        prev_dt = self._statistics.dt
        if prev_dt is not None:
            if (now - prev_dt).seconds < 60:  # TODO: Magic number
                return self._statistics

        query = db.select([func.count(Document.id)])  # pylint: disable=no-member
        total = await query.gino.scalar()
        ret = Stats(total=total)
        self._statistics = ret
        return ret


doc_service = DocumentsService()
