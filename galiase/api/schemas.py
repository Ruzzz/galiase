from datetime import datetime
from typing import Optional, List

from pydantic import BaseModel

from galiase.core.schemas import HttpUrlExSchema


class UrlStatusSchema(BaseModel):
    url: HttpUrlExSchema
    status: Optional[str]
    updated: Optional[datetime]


class StatsSchema(BaseModel):
    total: int = 0
    dt: Optional[datetime] = None


class DocumentSchema(BaseModel):
    id: int
    updated: Optional[datetime]
    url: str
    title: Optional[str]
    text: Optional[str]

    class Config:
        orm_mode = True


class DocumentsSchema(BaseModel):
    documents: List[DocumentSchema]
    total: int
