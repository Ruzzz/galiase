import logging
from pathlib import Path

from fastapi import FastAPI
from starlette.staticfiles import StaticFiles

from galiase import conf
from galiase.api.resources.api_v1 import api_v1_router
from galiase.api.resources.web_v1 import web_v1_router
from galiase.api.services import doc_service
from galiase.models.meta import db

_logger = logging.getLogger(__name__)
_ROOT_PATH = Path(__file__).parent.absolute()


def make_app():
    static_path = _ROOT_PATH / 'static'
    _logger.info('db: %s', conf.GALIASE_DB_URI)
    _logger.info('jobs server: %s, queue: %s',
                 conf.GALIASE_JOBS_URI,
                 conf.GALIASE_JOBS_QUEUE)
    _logger.debug('static path: %s', str(static_path))

    async def _db_init():
        await db.set_bind(conf.GALIASE_DB_URI)

    async def _db_close():
        await db.pop_bind().close()

    ret = FastAPI(
        title=conf.GALIASE_TITLE,
        openapi_url=f'{conf.GALIASE_API_PREFIX}/openapi.json',
        on_startup=[_db_init, doc_service.setup],
        on_shutdown=[doc_service.close, _db_close]
    )
    ret.include_router(web_v1_router)
    ret.include_router(api_v1_router, prefix=conf.GALIASE_API_PREFIX)
    ret.mount('/static', StaticFiles(directory=static_path), name='static')
    return ret


app = make_app()
