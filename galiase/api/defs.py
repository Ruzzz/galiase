from dataclasses import dataclass
from datetime import datetime, timezone
from typing import Optional


@dataclass
class Stats:
    total: int = 0
    dt: Optional[datetime] = datetime.now(timezone.utc)
