import logging
from pathlib import Path

from fastapi import APIRouter, Request
from starlette.responses import HTMLResponse, RedirectResponse
from starlette.templating import Jinja2Templates

from galiase import conf

_logger = logging.getLogger(__name__)
web_v1_router = APIRouter()

_ROOT_PATH = Path(__file__).parent.parent.absolute()
_TEMPLATES_PATH = _ROOT_PATH / 'templates'
_TEMPLATES = Jinja2Templates(directory=_TEMPLATES_PATH)
_logger.debug('templates path: %s', str(_TEMPLATES_PATH))


@web_v1_router.get('/favicon.ico', include_in_schema=False)
def favicon():
    return RedirectResponse(url='/static/favicon.ico', status_code=301)


async def home(request: Request):
    return _TEMPLATES.TemplateResponse('index.html', {'title': conf.GALIASE_TITLE, 'request': request})


for path in ['/', '/index']:
    web_v1_router.add_api_route(path, home, response_class=HTMLResponse, include_in_schema=False)
