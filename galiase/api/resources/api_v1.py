import logging
from dataclasses import asdict

from fastapi import APIRouter, Query, Depends

from galiase.api.schemas import UrlStatusSchema, DocumentsSchema, StatsSchema
from galiase.api.services import doc_service
from galiase.core.schemas import IndexUrlSchema

_logger = logging.getLogger(__name__)
api_v1_router = APIRouter()


@api_v1_router.api_route('/index', methods=['get', 'post'], response_model=UrlStatusSchema)
async def index(args: IndexUrlSchema = Depends(IndexUrlSchema.depends)):
    return await doc_service.index_url(**args.dict())


@api_v1_router.get('/stats', response_model=StatsSchema)
async def statistics():
    return asdict(await doc_service.statistics())


@api_v1_router.api_route('/search', methods=['get', 'post'], response_model=DocumentsSchema)
async def search(query: str = Query(...),
                 limit: int = Query(10, ge=3, le=80),
                 offset: int = Query(0, ge=0),
                 sort_rank: bool = Query(True),
                 text_limit: int = Query(200, ge=30, le=1000)):
    total = await doc_service.total(query)
    documents = await doc_service.search(
        query, limit=limit, offset=offset, sort_rank=sort_rank, text_limit=text_limit
    )
    return DocumentsSchema(documents=documents, total=total)
