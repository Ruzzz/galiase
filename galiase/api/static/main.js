'use strict';

Vue.directive('display', (el, binding) => {
    el.style.display = !!binding.value ? 'block' : 'none'
});

Vue.directive('display2', (el, binding) => {
    el.style.display = !!binding.value ? 'inline' : 'none'
});

let vm = new Vue({
    el: '#app',
    data: {
        searchOrIndexInput: '',
        lastSearchQuery: null,
        documents: null,
        documentsLimit: 20,
        statusBox: '',
        isIndexBox: false,
        paginationVisible: false,
        paginationPage: 0,
        paginationPages: [],
    },
    computed: {
        activeIndexTabClassObject: function () {
            return { 'active': this.isIndexBox }
        },
        activeSearchTabClassObject: function () {
            return { 'active': !this.isIndexBox }
        },
        documentTableVisible: function () {
            return this.documents && this.documents.length;
        },
        searchOrIndexPlaceholderText: function () {
            return this.isIndexBox ? 'Input URL' : 'Input query';
        }
    },
    watch: {
        searchOrIndexInput: function (newValue, oldValue) {
            if (!(this.isIndexBox || this.documentTableVisible)) {
                this.debouncedSearchOrIndex();
            }
        }
    },
    created: function () {
        this.debouncedSearchOrIndex = _.debounce(this.searchOrIndexFromInput, 500);
    },
    mounted: function () {
        this.updateStateFromUrl();
    },
    methods: {
        pagItemActiveClassObj: function (page) {
            return {'active': page === this.paginationPage}
        },
        prepareUrlText: function (url) {
            if (!url) return '';
            let parts = new URL(url)
            url = punycode.toUnicode(parts.hostname) + decodeURIComponent(parts.pathname);
            url = url.replace(/\/+$/, '');
            url = url.replaceAll('/', ' › ');
            return url;
        },
        updateStateFromUrl: function () {
            this.isIndexBox = window.location.pathname === '/index';

            if (!this.isIndexBox) {
                let query = this.getQueryFromUrl()
                if (query && this.searchOrIndexInput !== query) {
                    this.search(query);
                } else {
                    this.showStats();
                }
            } else {
                this.documents = null;
                this.showStats();
            }
        },
        getQueryFromUrl: function () {
            let hash = window.location.hash
            if (hash) {
                let ret = hash.substring(1)
                if (ret) {
                    return decodeURIComponent(ret)
                }
            }
            return null
        },
        searchOrIndexFromInput: function () {
            if (this.isIndexBox) {
                this.addIndex(this.searchOrIndexInput);
            } else {
                this.search(this.searchOrIndexInput);
            }
        },
        showStats: function () {
            let vm = this;
            axios.get(
                '/api/v1/stats'
            ).then(function (response) {
                let msg = 'Total documents in index: <b>' + response.data.total + '</b>';
                if (response.data.dt) {
                    msg = msg + '<p>' + response.data.dt
                }
                vm.statusBox = msg;
            }).catch(function (error) {
            })
        },
        addIndex: function (url) {
            if (!url) {
                return;
            }

            this.searchOrIndexInput = '';
            let vm = this;
            axios.get(
                '/api/v1/index', {
                    params: {
                        url: url,
                        depth: 1,
                        domain_only: true
                    }
            }).then(function (response) {
                let msg = 'Status: ' + response.data.status;
                if (response.data.updated) {
                    msg = msg + '<p>' + response.data.updated
                }
                vm.statusBox = msg;
            }).catch(function (error) {
                let detail = vm.parseErrorDetail(error);
                vm.statusBox = 'Error: ' + (detail ? detail : 'Unknown error');
                window.scrollTo(0,0);
            })
        },
        search: function (query) {
            this.search_(query, 0, this.documentsLimit, false, false);
        },
        loadPage: function (page) {
            this.search_(this.searchOrIndexInput, page, this.documentsLimit, false, true);
        },
        search_: function (query, page, limit, append, force) {
            force = append || force
            if (!query || query.length < 2 || (!force && this.lastSearchQuery === query)) {
                return;
            }
            query = query.trim()
            if (this.lastSearchQuery !== query) this.paginationPages = [];
            this.lastSearchQuery = query;
            window.location = '/#' + query;
            this.searchOrIndexInput = query;

            let vm = this;
            axios.get(
                '/api/v1/search', {
                    params: {
                        query: query,
                        offset: page * this.documentsLimit,
                        limit: limit,
                        sort_rank: true,
                        text_limit: 200
                    }
            }).then(function (response) {
                let documents = response.data.documents;
                if (append) {
                    vm.documents.push(...documents);
                } else {
                    vm.documents = documents;
                }

                let total = response.data.total;
                let count = documents.length;
                let paginationNeeded = (count < total) && (count <= vm.documentsLimit);

                vm.statusBox = 'Found <b>' + total + '</b> document(s)';

                vm.paginationVisible = paginationNeeded;
                if (paginationNeeded) {
                    let pagesTotal = Math.ceil(total / vm.documentsLimit);
                    const showMaxPages = 14;
                    if (!vm.paginationPages.length || pagesTotal > showMaxPages) {
                        let pagesCount = pagesTotal;
                        if (pagesCount > showMaxPages) pagesCount = showMaxPages;
                        let pagesStartAt = page - Math.trunc(pagesCount / 2);
                        if (pagesStartAt < 0) pagesStartAt = 0;
                        if (pagesStartAt + pagesCount > pagesTotal) pagesStartAt = pagesTotal - pagesCount;
                        vm.paginationPages = [...Array(pagesCount).keys()].map(i => i + pagesStartAt);
                    }
                    vm.paginationPage = page;
                }

                window.scrollTo(0,0);
            }).catch(function (error) {
                if (!append) vm.documents = null;

                let msg = 'We\'re sorry, we\'re not able to retrieve information at the moment, ' +
                    'please try back later.';
                let detail = vm.parseErrorDetail(error);
                if (detail) {
                    msg = msg + '<p>' + detail;
                }
                vm.statusBox = msg;
                window.scrollTo(0,0);
            })
        },
        parseErrorDetail: function (error) {
            console.log(error);

            if (error && error.response && error.response.data) {
                let data = error.response.data;
                let ret = data.detail && data.detail.length ? data.detail[0].msg : data.msg;
                if (ret) {
                    return ret;
                }
            }
            return null;
        }
    }
})
