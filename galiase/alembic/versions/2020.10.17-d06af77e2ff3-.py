"""empty message

Revision ID: d06af77e2ff3
Revises: 
Create Date: 2020-10-17 01:13:33.529950

"""
import sqlalchemy as sa
import sqlalchemy_utils
from alembic import op
from sqlalchemy_searchable import sync_trigger, drop_trigger

# revision identifiers, used by Alembic.

revision = 'd06af77e2ff3'
down_revision = None
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.create_table('documents',
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('updated', sa.DateTime(timezone=True), server_default=sa.text('now()'), nullable=False),
    sa.Column('url', sa.String(), nullable=False),
    sa.Column('title', sa.String(), nullable=True),
    sa.Column('text', sa.String(), nullable=True),
    sa.Column('last_error', sa.String(), nullable=True),
    sa.Column('text_fts', sqlalchemy_utils.types.ts_vector.TSVectorType(), nullable=True),
    sa.PrimaryKeyConstraint('id', name=op.f('pk_documents')),
    sa.UniqueConstraint('url', name=op.f('uq_documents_url'))
    )
    op.create_index('idx_documents_url', 'documents', ['url'], unique=True)
    # ### end Alembic commands ###

    conn = op.get_bind()
    sync_trigger(conn, 'documents', 'text_fts', ['title', 'text'])


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_index('idx_documents_url', table_name='documents')
    op.drop_table('documents')
    # ### end Alembic commands ###

    conn = op.get_bind()
    drop_trigger(conn, 'documents', 'text_fts')
