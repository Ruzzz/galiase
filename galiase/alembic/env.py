from alembic import context
from logging.config import fileConfig  # pylint: disable=wrong-import-order

from sqlalchemy import create_engine

from galiase import conf
from galiase.models.models import *  # pylint: disable=wildcard-import,unused-wildcard-import
from galiase.models.meta import db

config = context.config  # pylint: disable=no-member
target_metadata = db
fileConfig(config.config_file_name)


def run_migrations():
    with create_engine(conf.GALIASE_DB_URI).connect() as connection:
        context.configure(  # pylint: disable=no-member
            connection=connection,
            target_metadata=target_metadata,
            compare_server_default=True,
        )
        with context.begin_transaction():  # pylint: disable=no-member
            context.run_migrations()  # pylint: disable=no-member


run_migrations()
