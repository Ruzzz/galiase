import os

GALIASE_DEVELOPMENT = False
GALIASE_TESTING = False

GALIASE_TITLE = 'GaliaSE'
GALIASE_API_PREFIX = '/api/v1'

GALIASE_DB_URI = os.environ.get('GALIASE_DB_URI')
GALIASE_JOBS_URI = os.environ.get('GALIASE_JOBS_URI')
GALIASE_JOBS_QUEUE = os.environ.get('GALIASE_JOBS_QUEUE', 'urls')
GALIASE_SPIDER_CONSUMERS_COUNT = int(os.environ.get('GALIASE_SPIDER_CONSUMERS_COUNT', 20))
GALIASE_SPIDER_CONTENT_LEN_LIMIT = int(os.environ.get('GALIASE_SPIDER_CONTENT_LEN_LIMIT', 1024 * 1024))  # 1MB
GALIASE_SPIDER_ALLOW_REDIRECTS = True

GALIASE_LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'root': {
        'level': 'WARNING',
        'handlers': ['console']
    },
    'formatters': {
        'simple': {
            'format': '%(asctime)s | %(name)s | %(levelname)s | %(message)s'
        },
        'verbose': {
            'format': '%(asctime)s | %(process)d | %(name)s | %(levelname)s | '
                      '%(filename)s:%(lineno)d | %(funcName)s() | %(message)s',
            'datefmt': '%d/%b/%Y %H:%M:%S'
        }
    },
    'handlers': {
        'null': {
            'level': 'DEBUG',
            'class': 'logging.NullHandler'
        },
        'console': {
            'level': 'DEBUG',
            'class': 'logging.StreamHandler',
            'formatter': 'verbose'
        }
    },
    'loggers': {
        'galiase': {
            'handlers': ['console'],
            'propagate': False,
            'level': 'INFO',
        },
        'fastapi': {
            'handlers': ['console'],
            'propagate': False,
            'level': 'INFO',
        },
        'uvicorn': {
            'handlers': ['console'],
            'propagate': False,
            'level': 'INFO',
        },
        'aioamqp': {
            'handlers': ['console'],
            'propagate': False,
            'level': 'INFO',
        },
    }
}
