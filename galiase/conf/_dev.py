from ._base import GALIASE_LOGGING, GALIASE_DB_URI, GALIASE_JOBS_URI

GALIASE_DEVELOPMENT = True
if not GALIASE_DB_URI:
    GALIASE_DB_URI = 'postgresql://dev:dev@localhost:5432/galiase'
if not GALIASE_JOBS_URI:
    GALIASE_JOBS_URI = 'amqp://dev:dev@localhost/'
GALIASE_SPIDER_CONSUMERS_COUNT = 1
# GALIASE_LOGGING['root']['level'] = 'DEBUG'
GALIASE_LOGGING['loggers']['galiase']['level'] = 'DEBUG'  # type: ignore
