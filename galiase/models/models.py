from sqlalchemy import Column, Integer, String, Index, DateTime, func
from sqlalchemy_searchable import make_searchable
from sqlalchemy_utils import TSVectorType

from galiase.models.meta import db

__all__ = ['Document']


make_searchable(db)


class Document(db.Model):
    __tablename__ = 'documents'

    id = Column(Integer, primary_key=True)
    updated = Column(DateTime(timezone=True), server_default=func.now(), onupdate=func.now(), nullable=False)
    url = Column(String, nullable=False, unique=True)
    title = Column(String, nullable=True)
    text = Column(String, nullable=True)
    last_error = Column(String, nullable=True)

    # - alembic -
    # def upgrade():
    #   conn = op.get_bind()
    #   sync_trigger(conn, 'documents', 'text_fts', ['title', 'text'])
    # def downgrade():
    #   conn = op.get_bind()
    #   drop_trigger(conn, 'documents', 'text_fts')
    text_fts = Column(TSVectorType('title', 'text', regconfig='english', weights={'title': 'A', 'text': 'B'}))

    __table_args__ = (
        Index('idx_documents_url', url, unique=True),
    )
