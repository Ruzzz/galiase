import asyncio
import logging

from galiase import conf
from galiase.core import on_startups, on_shutdowns, init_on_terminate, GracefulExit
from galiase.models.meta import db
from galiase.spider.worker import PagesConsumer

_logger = logging.getLogger('galiase.spider.app')


async def _db_init():
    await db.set_bind(conf.GALIASE_DB_URI)


async def _db_close():
    await db.pop_bind().close()


def main():
    loop = asyncio.get_event_loop()
    init_on_terminate(loop)

    try:
        _logger.info('starting')
        _logger.info('db: %s', conf.GALIASE_DB_URI)
        _logger.info('jobs server: %s, queue: %s, consumers: %d',
                     conf.GALIASE_JOBS_URI,
                     conf.GALIASE_JOBS_QUEUE,
                     conf.GALIASE_SPIDER_CONSUMERS_COUNT)

        on_startups.append(_db_init)

        consumers = []
        for _ in range(conf.GALIASE_SPIDER_CONSUMERS_COUNT):
            consumer = PagesConsumer(conf.GALIASE_JOBS_URI, conf.GALIASE_JOBS_QUEUE, loop=loop)
            consumers.append(consumer)
            on_startups.append(consumer.setup)
            on_shutdowns.append(consumer.close)

        on_shutdowns.append(_db_close)

        loop.run_until_complete(on_startups.run())

        for consumer in consumers:
            loop.create_task(consumer())

        loop.run_forever()

    except (KeyboardInterrupt, GracefulExit):
        pass
    except Exception as err:
        _logger.critical(str(err))
        raise err
    finally:
        _logger.info('shutting down')
        loop.run_until_complete(on_shutdowns.run())

    loop.close()


if __name__ == '__main__':
    main()
