import asyncio
import logging
import re
from dataclasses import dataclass
from datetime import datetime, timezone
from typing import Optional, Set
from urllib.parse import urlparse

import aiohttp
import async_timeout
from asyncpg import PostgresError
from bs4 import BeautifulSoup
from pydantic import ValidationError

from galiase import conf
from galiase.core.jobs import AmqpJobsConsumerAbstract
from galiase.core.schemas import IndexUrlSchema
from galiase.core.ua import random_user_agent
from galiase.core.urls import UrlValidator
from galiase.models.models import Document

_logger = logging.getLogger(__name__)


class SkipLoadError(Exception):
    pass


class LoadLaterError(Exception):
    pass


class ContentLoader:
    TIMEOUT = 60  # In seconds

    _VALID_CONTENT_TYPE = {
        'text/html',
        'text/plain',
    }

    @classmethod
    def gen_http_headers(cls, generate_user_agent=False):
        headers = {
            'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8',
            'Accept-Encoding': 'gzip, deflate',
            'Accept-Language': 'en-US,en;q=0.9',
            'Connection': 'keep-alive',
            'Referer': 'https://google.com',
        }
        if generate_user_agent:
            headers['User-Agent'] = random_user_agent()
        return headers

    @classmethod
    def make_http_session(cls, loop=None):
        if not loop:
            loop = asyncio.get_event_loop()
        connector = aiohttp.TCPConnector(verify_ssl=False)
        headers = cls.gen_http_headers(generate_user_agent=True)
        return aiohttp.ClientSession(connector=connector, headers=headers, loop=loop,
                                     conn_timeout=cls.TIMEOUT, read_timeout=cls.TIMEOUT)

    async def __call__(self, url: str, loop=None) -> str:
        # TODO: Detect lang

        async with self.make_http_session() as session:
            try:
                async with async_timeout.timeout(self.TIMEOUT, loop=loop):
                    async with session.get(url=url, allow_redirects=conf.GALIASE_SPIDER_ALLOW_REDIRECTS) as response:
                        if response.status != 200:
                            raise SkipLoadError(f'invalid response status code: {response.status}')

                        invalid_content_type = (
                            not response.content_type or
                            response.content_type.split(';')[0] not in self._VALID_CONTENT_TYPE
                        )
                        if invalid_content_type:
                            raise SkipLoadError(f'invalid response content type: {response.content_type}')

                        if response.content_length is not None:
                            length = int(response.content_length)
                        else:
                            length = int(response.content.total_bytes)
                        if length > conf.GALIASE_SPIDER_CONTENT_LEN_LIMIT:
                            raise SkipLoadError(f'response content too big : {length}')

                        return await response.text(errors='ignore')

            except asyncio.TimeoutError:
                raise LoadLaterError('timeout')


@dataclass
class ContentData:
    title: str
    text: str
    links: Optional[Set[str]] = None


class ContentParser:
    _LINK_RE = re.compile(r' href=\"(.+?)\"')
    _TITLE_RE = re.compile(r'<title>(.+?)</title>', flags=re.DOTALL)

    @classmethod
    def cleanup_html(cls, html: str) -> str:
        if not html:
            return ''
        html = html.strip()
        if not html:
            return ''

        bs = BeautifulSoup(html, 'lxml')
        for script in bs(['script', 'style']):
            script.extract()
        return bs.get_text(' ', True)

    def __call__(self, content: str, *,
                 parse_links: bool,
                 domain_only: str = None,
                 source_url: str = None) -> ContentData:
        links = None
        if parse_links:
            url_validator = UrlValidator(source_url)
            links = set(x.group(1) for x in self._LINK_RE.finditer(content) if x)
            links = set(map(url_validator, links))  # type: ignore
            links = set(filter(bool, links))
            if domain_only:
                links = set(filter(lambda x: urlparse(x).hostname == domain_only, links))

        title_m = self._TITLE_RE.search(content)
        title = title_m.group(1) if title_m else ''
        if title:
            title = title.strip().replace('\n', '')
            title = self.cleanup_html(title)

        text = self.cleanup_html(content)
        return ContentData(title, text, links)


class PagesConsumer(AmqpJobsConsumerAbstract):
    valid_content_type = None

    content_loader = ContentLoader()
    content_parser = ContentParser()

    async def _on_job(self, raw_data: dict) -> bool:  # pylint: disable=arguments-differ
        try:
            data: IndexUrlSchema = IndexUrlSchema.validate(raw_data)
        except ValidationError as err:
            _logger.error('invalid request data: %s', str(err).replace('\n', ', '))
            return True

        _logger.debug(
            'process url: %s with depth: %d, domain only: %d, skip recently: %d',
            data.url, data.depth, data.domain_only, data.skip_recently
        )

        if data.skip_recently and await self.check_recently_loaded(data.url):
            error_msg = 'skip recently loaded'
            await self.create_or_update_doc_error(data.url, error_msg)
            _logger.debug(error_msg)
            return True

        # load content
        try:
            content = await self.content_loader(data.url)
        except (LoadLaterError, SkipLoadError) as err:
            # TODO: Process LoadLaterError
            await self.create_or_update_doc_error(data.url, str(err))
            return True

        # parse content
        content_data = self.content_parser(
            content,
            parse_links=data.depth > 0,
            domain_only=urlparse(data.url).hostname if data.domain_only else None,
            source_url=data.url
        )
        if content_data.links:
            _logger.debug('found %d links', len(content_data.links))
            for link in content_data.links:
                await self.publish({
                    'url': link,
                    'depth': data.depth - 1,
                })

        if not content_data.title:
            await self.create_or_update_doc_error(data.url, 'skip doc with empty title')
            return True
        if not content_data.text:
            await self.create_or_update_doc_error(data.url, 'skip doc with empty text')
            return True

        try:
            await self.create_or_update_doc(data.url, content_data.title, content_data.text)
        except PostgresError as err:
            _logger.debug(str(err).replace('\n', ', '))

        return True

    @classmethod
    async def check_recently_loaded(cls, url: str) -> bool:
        doc = await Document.query.where(Document.url == url).gino.one_or_none()
        if doc:
            dt_diff = datetime.now(timezone.utc) - doc.updated
            return dt_diff.seconds < 10 * 60  # TODO: Magic number
        return False

    @classmethod
    async def create_or_update_doc(cls, url: str, title: str, text: str):
        _logger.debug('doc processed, len: %d', len(text))
        doc = await Document.query.where(Document.url == url).gino.one_or_none()
        if doc is None:
            await Document.create(url=url, title=title, text=text)
        else:
            q = doc.update(title=title, text=text, last_error=None)
            await q.apply()

    @classmethod
    async def create_or_update_doc_error(cls, url: str, status: str):
        _logger.error(status)

        doc = await Document.query.where(Document.url == url).gino.one_or_none()
        if doc is None:
            await Document.create(url=url, last_error=status)
        else:
            q = doc.update(last_error=status)
            await q.apply()
