FROM python:3.8-buster
COPY requirements-api.txt /app/
WORKDIR /app
RUN pip cache info && \
    pip install -U pip wheel && \
    pip install alembic && \
    pip install -r requirements-api.txt
COPY . .
ENV GALIASE_CONF "prod"
CMD make db-migrate && make run-api
