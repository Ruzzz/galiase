FROM python:3.8-buster
COPY requirements-spider.txt /app/
WORKDIR /app
RUN pip cache info && \
    pip install -U pip wheel && \
    pip install -r requirements-spider.txt
COPY . .
ENV GALIASE_CONF "prod"
CMD make run-spider
