-include pre_local.mk

run-api:
	PYTHONPATH=. uvicorn --host 0.0.0.0 --port 8000 galiase.api.app:app

run-spider:
	python -m galiase.spider.app

test:
	pytest --disable-pytest-warnings --cov=galiase tests

check:
	pylint galiase && mypy galiase

db-migrate:
	PYTHONPATH=. alembic upgrade head

db-add:
	PYTHONPATH=. alembic revision --autogenerate

db-undo:
	PYTHONPATH=. alembic downgrade -1

up:
	docker-compose -f dev_env/galiase/docker-compose.yml up --build

down:
	docker-compose -f dev_env/galiase/docker-compose.yml down

-include post_local.mk
