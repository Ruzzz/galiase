import pytest

from galiase.core.urls import prepare_url


@pytest.mark.parametrize(
    'url, source_url, expected',
    (
        ('/path2', 'http://www.example.com:8080/path1', 'http://www.example.com:8080/path2'),
        ('www.example.com:8080/path2', 'https://www.example.com:8080/path1', 'https://www.example.com:8080/path2'),
        ('www.example.com:8080/path2', 'https://www.example.com/path1', 'http://www.example.com:8080/path2'),

        ('www.example.com/path', None, 'http://www.example.com/path'),
        ('/path', None, '/path'),

        ('http://www.Example.com', None, 'http://www.example.com'),
        ('http://www.example.com/#part', None, 'http://www.example.com/'),
        ('http://www.example.com/path?query=value#part', None, 'http://www.example.com/path?query=value'),
    )
)
def test_prepare_url(url: str, source_url, expected: str):
    assert prepare_url(url, source_url) == expected
