## About

Example of Web Spider + Web UI based on:

- fastapi
- rabbitmq
- postgresql + full-text search + gino
- vue.js

## Run in docker

- `make up`

## Run on host

- Disable api & spider services in `devenv/galiase/docker-compose.yml`
- Prepare virtual environment
- `make up && make db-migrate && make run-api && make run-spider`

## Links

- Web: http://127.0.0.1:8000
- Swagger: http://127.0.0.1:8000/docs
- OpenAPI: http://127.0.0.1:8000/redoc
- RabbitMQ: http://127.0.0.1:15672 `dev:dev`
